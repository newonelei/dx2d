#pragma once

#include <winsock.h>

class Client
{
public:
	Client(UINT port);
	~Client();
	bool Send(const char* message);

	void CloseSocket();

private:
	SOCKET m_socket;

	UINT m_port;

	SOCKADDR_IN m_udpServerAddr;
};
#pragma once

#include "gameobject.h"
#include "knowledge.h"
#include <d2d1.h>
#include <d2d1helper.h>

class Stick : public GameObject {
public:
    Stick(unsigned int id, int width, int height, D2D1::ColorF color, Knowledge& knowledge);
    void Shoot();
    void Bounce();

    void Update(double dt) override;
    void Render(const RenderContext &) override;

    FLOAT GetWidth() const { return m_width; }

    FLOAT GetHeight() const { return m_height; }

private:
    FLOAT        m_height;
    FLOAT        m_width;
    D2D1::ColorF m_color;

	Knowledge& knowledge;
};
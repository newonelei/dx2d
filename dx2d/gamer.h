#pragma once
#include <keyboard.h>

class Gamer {
public:
    void UpdateInput(double dt, DirectX::Keyboard::State state);
	unsigned char GetInputCode(DirectX::Keyboard::State state);

    int  GetInputDir() const;
    bool IsShootBall() const;
    bool IsResetGame() const;
    bool IsPauseGame() const;

protected:

private:
    struct KeyState {
        bool keyLeftDown;
        bool keyRightDown;
        bool keySpaceDown;
        bool keyResetDown;
        bool keyPauseDown;
    };

    struct KeyState m_keyState;
};
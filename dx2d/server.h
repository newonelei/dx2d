#pragma once
#include "winsock.h"
#include <vector>
class Server
{
public:
	Server(UINT port);
	~Server();

	void Update();

	void Read();
	void Parse();

	void CloseSocket();
private:
	SOCKET m_socket;

	UINT m_port;

	std::vector<char*> m_buffers;
};
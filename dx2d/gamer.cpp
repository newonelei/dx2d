#include "gamer.h"

void Gamer::UpdateInput(double dt, DirectX::Keyboard::State state) {
    m_keyState.keyLeftDown  = state.IsKeyDown(DirectX::Keyboard::Left);
    m_keyState.keyRightDown = state.IsKeyDown(DirectX::Keyboard::Right);
    m_keyState.keySpaceDown = state.IsKeyDown(DirectX::Keyboard::Space);
    m_keyState.keyResetDown = state.IsKeyDown(DirectX::Keyboard::R);
    m_keyState.keyPauseDown = state.IsKeyDown(DirectX::Keyboard::P);
}

unsigned char Gamer::GetInputCode(DirectX::Keyboard::State state)
{
	if (state.IsKeyDown(DirectX::Keyboard::Left))
		return DirectX::Keyboard::Left;
	else if (state.IsKeyDown(DirectX::Keyboard::Right))
		return DirectX::Keyboard::Right;
	else if (state.IsKeyDown(DirectX::Keyboard::Space))
		return DirectX::Keyboard::Space;
	else if (state.IsKeyDown(DirectX::Keyboard::R))
		return DirectX::Keyboard::R;
	else if (state.IsKeyDown(DirectX::Keyboard::P))
		return DirectX::Keyboard::P;
	else
		return '\0';
}

int Gamer::GetInputDir() const {
    int left  = m_keyState.keyLeftDown ? -1 : 0;
    int right = m_keyState.keyRightDown ? 1 : 0;
    return left + right;
}

bool Gamer::IsShootBall() const {
    return m_keyState.keySpaceDown;
}

bool Gamer::IsResetGame() const {
    return m_keyState.keyResetDown;
}

bool Gamer::IsPauseGame() const {
    return m_keyState.keyPauseDown;
}

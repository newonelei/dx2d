#pragma once
#include "message.h"

class HeartBeatMessage : public Message
{
public:
	HeartBeatMessage() : Message(Message::CONTROL)
	{
	}

	void Serialise() final
	{
	}
};

class WelcomeMessage : public Message
{
public:
	WelcomeMessage(unsigned short id, bool is_server) 
		: Message(Message::CONTROL)
		, participant_id_(id)
		, is_server_(is_server)
	{
	}

	void Serialise() final
	{
	}

private:
	unsigned short participant_id_;
	bool is_server_;
};
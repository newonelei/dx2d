#pragma once

#include "ball.h"
#include "gamer.h"
#include "render_context.h"
#include "stick.h"

class Scene {
public:
    Scene(const RenderContext & render_context);

    void Init();
    void Update(double dt);
    void Render();

    void SetStickOneGamer(const Gamer * gamer);
    void SetStickTwoGamer(const Gamer * gamer);

private:
    Stick m_stickOne;
    Stick m_stickTwo;

    Ball m_ball;

    Knowledge m_knowledge;

    const RenderContext & m_renderContext;
};
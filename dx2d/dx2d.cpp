﻿#include "dx2d.h"
#include "messages.h"
#include <chrono>

D2DDemo::D2DDemo(bool isServer)
    : m_hwnd(NULL)
    , m_pDirect2dFactory(NULL)
    , m_pRenderTarget(NULL)
    , m_keyboard(std::make_unique<DirectX::Keyboard>())
    , m_scene(m_renderContext) {
    
	if (isServer) {
        m_server = new Server(10003);
    } else {
        m_client = new Client(10003);
    }
}

D2DDemo::~D2DDemo() {
    ReleaseDeviceDependentResources();

    if (m_pDirect2dFactory) {
        m_pDirect2dFactory->Release();
        m_pDirect2dFactory = NULL;
    }

	if(m_server)
	{
		delete m_server;
		m_server = nullptr;
	}

	if(m_client)
	{
		delete m_client;
		m_client = nullptr;
	}
}

HRESULT D2DDemo::Initialize() {
    HRESULT hr = CreateDeviceIndependentResources();
    if (FAILED(hr)) return hr;

    // register the window class
    WNDCLASSEX wnd_class    = {};
    wnd_class.cbSize        = sizeof(WNDCLASSEX);
    wnd_class.style         = CS_HREDRAW | CS_VREDRAW;
    wnd_class.lpfnWndProc   = D2DDemo::WndProc;
    wnd_class.cbClsExtra    = 0;
    wnd_class.cbWndExtra    = sizeof(LONG_PTR);
    wnd_class.hInstance     = HINST_THISCOMPONENT;
    wnd_class.hbrBackground = NULL;
    wnd_class.lpszMenuName  = NULL;                               // 窗口菜单
    wnd_class.hCursor       = LoadCursor(NULL, IDI_APPLICATION);  // 游戏鼠标
    wnd_class.lpszClassName = "DX2DClass";                        // Windows窗口类 名字

    RegisterClassEx(&wnd_class);

    m_hwnd = CreateWindow("DX2DClass",  // windows 窗口类型名
                          "2D Game",    // windows窗口名
                          WS_OVERLAPPEDWINDOW,
                          CW_USEDEFAULT,        // x 坐标
                          CW_USEDEFAULT,        // y 坐标
                          0,                    // 宽度
                          0,                    // 高度
                          NULL,                 // 父窗口ID
                          NULL,                 // 菜单
                          HINST_THISCOMPONENT,  // 当前module的instance ID
                          this);

    if (m_hwnd) {
        float dpi = GetDpiForWindow(m_hwnd);

        SetWindowPos(m_hwnd,
                     NULL,
                     NULL,
                     NULL,
                     static_cast<int>(ceil(640.f * dpi / 96.f)),
                     static_cast<int>(ceil(480.f * dpi / 96.f)),
                     SWP_NOMOVE);

        ShowWindow(m_hwnd, SW_SHOWNORMAL);
        UpdateWindow(m_hwnd);
    }
    return hr;
}

int D2DDemo::MessageLoop() {
    MSG msg = {};
    while (msg.message != WM_QUIT) {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        } else {
            GameLoop();
            OnRender();
        }
    }

    return (int) msg.wParam;
}

void D2DDemo::OnRender() {
    HRESULT hr = CreateDeviceDependentResources();

    if (SUCCEEDED(hr)) {
        D2D1_SIZE_F render_target_size = m_pRenderTarget->GetSize();

        m_pRenderTarget->BeginDraw();
        m_pRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
        {
            // 清空渲染区域的背景颜色
            m_pRenderTarget->Clear(D2D1::ColorF(D2D1::ColorF::Black));

            // 画画
            FLOAT width                          = render_target_size.width;
            FLOAT height                         = render_target_size.height;

            m_renderContext.render_target_width  = width;
            m_renderContext.render_target_height = height;

            m_scene.Render();

            /*
    FLOAT y_center     = height / 2;
    FLOAT x_center     = width / 2;
    FLOAT y_offset     = 5;
    FLOAT x_offset     = 20;
    FLOAT stick_height = 10;
    FLOAT ball_radius  = 7.5;

    m_pSolidBrush->SetColor(D2D1::ColorF(D2D1::ColorF::Yellow, 1.0f));
    // m_pRenderTarget->DrawLine(D2D1::Point2F(0, y_center), D2D1::Point2F(width, y_center), m_pSolidBrush);
    D2D1_RECT_F stick_one_rect
        = D2D1::Rect(width / 2 - x_offset, y_offset, width / 2 + x_offset, y_offset + stick_height);
    m_pRenderTarget->FillRectangle(stick_one_rect, m_pSolidBrush);

    D2D1_RECT_F stick_two_rect
        = D2D1::Rect(width / 2 - x_offset, height - y_offset - stick_height, width / 2 + x_offset, height - y_offset);
    m_pRenderTarget->FillRectangle(stick_two_rect, m_pSolidBrush);

    // ball
    m_pSolidBrush->SetColor(D2D1::ColorF(D2D1::ColorF::Blue, 1.0f));
    D2D1_ELLIPSE ball = D2D1::Ellipse(D2D1::Point2F(x_center, y_center), ball_radius, ball_radius);
    m_pRenderTarget->FillEllipse(ball, m_pSolidBrush);
            */

            m_pRenderTarget->EndDraw();
        }
    }
}

void D2DDemo::OnResize(UINT width, UINT height) {
    if (m_pRenderTarget) { m_pRenderTarget->Resize(D2D1::SizeU(width, height)); }
}

void D2DDemo::OnPaint() {}

void D2DDemo::GameInit() {
    m_timer.Start();

    m_scene.Init();
    m_scene.SetStickOneGamer(&m_gamer);
    m_scene.SetStickTwoGamer(&m_gamer);

    m_gameState = EGameState::Unstarted;
}

void D2DDemo::TickNetwork()
{
    if (m_server) {
        m_server->Update();
    } else {
        m_client->Send("hello");
    }
}

void D2DDemo::GameLoop() {
    m_timer.Tick();

    // 1. input
    m_gamer.UpdateInput(0.0, m_keyboard->GetState());

    // update gamer object
    double dt = m_timer.DeltaTime();  // / 1000.0;
    switch (m_gameState) {
    case EGameState::Unstarted: {
		WelcomeMessage welcome_message(m_server ? 0 : 1, m_server);

        bool is_shoot_ball = m_gamer.IsShootBall();
        if (is_shoot_ball) m_gameState = EGameState::Started;
        break;
    }
    case EGameState::Started:
        m_scene.Update(dt);
		TickNetwork();
        if (m_gamer.IsPauseGame()) m_gameState = EGameState::Paused;
        break;

    case EGameState::Paused:
        if (m_gamer.IsPauseGame()) m_gameState = EGameState::Started;
        break;

    case EGameState::Exited:
        break;
    }

    if (m_gamer.IsResetGame()) {
        m_gameState = EGameState::Unstarted;
        m_scene.Init();
    }
}

LRESULT D2DDemo::WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
    HRESULT result = 0;

    // 创建窗口消息，必须处理，否则窗口会自动销毁
    if (message == WM_NCCREATE) {
        LPCREATESTRUCT pcs      = (LPCREATESTRUCT) lParam;
        D2DDemo *      pDemoApp = (D2DDemo *) pcs->lpCreateParams;

        // 设置demo app对象到窗口句柄hwnd对应的userdata结构里面去，为了后面我们可以访问这个demo app
        //  因为只有在消息 WM_NCCREATE的时候，才会在lParam里面包括LPCREATESTRUCT指针对象，里面包括了lpCreateParams指针
        ::SetWindowLongPtrW(hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pDemoApp));
        result = 1;
    } else {
        D2DDemo * demo_app = reinterpret_cast<D2DDemo *>(static_cast<LONG_PTR>(::GetWindowLongPtrW(hwnd, GWLP_USERDATA)));

        bool wasHandled    = false;
        if (demo_app) {
            switch (message) {
            case WM_ACTIVATE:
            case WM_ACTIVATEAPP:
                DirectX::Keyboard::ProcessMessage(message, wParam, lParam);
                break;
            case WM_SIZE: {
                // 窗口缩放和移动，都会触发这个消息
                UINT width  = LOWORD(lParam);
                UINT height = HIWORD(lParam);
                demo_app->OnResize(width, height);
                result     = 0;
                wasHandled = true;
                break;
            }
            case WM_DISPLAYCHANGE: {
                // 显示分辨率发生变化

                // 重画整个窗口区域
                InvalidateRect(hwnd, NULL, false);
                result     = 0;
                wasHandled = true;
                break;
            }
            case WM_PAINT: {
                demo_app->OnRender();

                // 通知windows这个区域已经更新过了，不用再update了
                ValidateRect(hwnd, NULL);

                result     = 0;
                wasHandled = true;
                break;
            }
            case WM_SYSKEYDOWN:  // alt, ctrl, shift, tab, enter
            case WM_SYSKEYUP:    // alt, ctrl, shift, tab, enter
            case WM_KEYDOWN:
            case WM_KEYUP:
                DirectX::Keyboard::ProcessMessage(message, wParam, lParam);
                break;

            case WM_DESTROY: {
                PostQuitMessage(0);
                result     = 1;
                wasHandled = true;
                break;
            }
            }

            if (!wasHandled) {
                // 其他消息留给windows默认的消息处理函数处理
                result = DefWindowProc(hwnd, message, wParam, lParam);  // add this
            }
        }
    }
    return result;
}

HRESULT D2DDemo::CreateDeviceIndependentResources() {
    HRESULT hr = S_OK;
    hr         = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_pDirect2dFactory);
    return hr;
}

HRESULT D2DDemo::CreateDeviceDependentResources() {
    RECT rc;
    GetClientRect(m_hwnd, &rc);

    D2D1_SIZE_U size = D2D1::SizeU(rc.right - rc.left, rc.bottom - rc.top);

    HRESULT hr       = m_pDirect2dFactory->CreateHwndRenderTarget(
        D2D1::RenderTargetProperties(), D2D1::HwndRenderTargetProperties(m_hwnd, size), &m_pRenderTarget);
    if (FAILED(hr)) {
        // failed to create render target
        return hr;
    }

    hr = m_pRenderTarget->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Yellow, 1.0f), &m_pSolidBrush);
    if (FAILED(hr)) {
        // failed to create solid brush
        return hr;
    }

    m_renderContext.render_target = m_pRenderTarget;
    m_renderContext.brush         = m_pSolidBrush;

    return hr;
}

void D2DDemo::ReleaseDeviceDependentResources() {
    if (m_pRenderTarget) {
        m_pRenderTarget->Release();
        m_pRenderTarget = NULL;
    }

    if (m_pSolidBrush) {
        m_pSolidBrush->Release();
        m_pSolidBrush = NULL;
    }
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd) {
	LPWSTR* argv;
	int argc;

	argv = CommandLineToArgvW(GetCommandLineW(), &argc);
	if(argv == nullptr)
	{
		// failed to parse command line arguments
		return 1;
	}

	bool is_server = false;
	if(argc > 1 && _wcsicmp(argv[1], L"server") == 0)
	{
		is_server = true;
	}

    D2DDemo app(is_server);
    if (SUCCEEDED(app.Initialize())) {
        app.GameInit();
        app.MessageLoop();
    }

    return 0;
}

void D2DDemo::GameStart() {
    m_gameState = EGameState::Started;
}

void D2DDemo::GamePause() {
    m_gameState = EGameState::Paused;
}

void D2DDemo::GameExit() {
    m_gameState = EGameState::Exited;
}
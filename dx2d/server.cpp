﻿#include "server.h"
#include "log.h"
#include <stdio.h>

#define WIN
#define MAXRECVSTRING 255 /* Longest string to receive */

Server::Server(UINT port) {
#ifdef WIN
    WORD    version = MAKEWORD(2, 2);
    WSADATA wsa_data;
    // initialize winsock
    WSAStartup(version, &wsa_data);
#endif
    m_socket = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (m_socket == -1) {
        const char * error_msg = "failed to create a socket for server";
        Log::Output(error_msg);
        return;
    }

    m_port = port;
    SOCKADDR_IN udp_server_addr;
    memset(&udp_server_addr, 0, sizeof(PSOCKADDR_IN));
    udp_server_addr.sin_family      = AF_INET;
    udp_server_addr.sin_port        = htons(port);
    udp_server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(m_socket, (SOCKADDR *) &udp_server_addr, sizeof(SOCKADDR_IN))) {
        const char * error_msg = "failed to bind socket.";
        Log::Output(error_msg);
        CloseSocket();
        return;
    }
}

// 析构函数
Server::~Server() {
    for (int i = 0; i < m_buffers.size(); ++i) {
        char * ptr = m_buffers[i];
        delete ptr;
        m_buffers[i] = NULL;
    }
    CloseSocket();
#ifdef WIN
    WSACleanup();
#endif
}

void Server::Update() {
    Read();
    Parse();
}

void Server::Read() {
    fd_set         fds;
    struct timeval timeout;
    timeout.tv_sec  = 0;
    timeout.tv_usec = 100;

    FD_ZERO(&fds);
    FD_SET(m_socket, &fds);

    int rc = select(sizeof(fds), &fds, NULL, NULL, &timeout);
    if (rc > 0) {
        const int   buffer_size = 1024;
        char        rbuf[buffer_size];
        SOCKADDR_IN client_addr;
        int         len        = sizeof(SOCKADDR_IN);
        int         bytes_read = recvfrom(m_socket, rbuf, buffer_size, 0, (sockaddr *) &client_addr, &len);
        if (bytes_read > 0) {
            char * copy_buffer = new char[bytes_read];
            memcpy(copy_buffer, rbuf, bytes_read);
            m_buffers.push_back(copy_buffer);
        }
    }
}

void Server::Parse() {
    // to do
}

void Server::CloseSocket() {
    if (m_socket != -1) {
        if (closesocket(m_socket) < 0) Log::Output("Error: close socket failed");

        m_socket = -1;
    }
}

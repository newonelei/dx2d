#include "gameobject.h"

GameObject::GameObject(unsigned int id)
    : m_gamer(nullptr) {
    m_id.id = id;
}

void GameObject::Move(double dt) {
    // todo: easy in/out later
    this->m_pos = DirectX::XMFLOAT2(m_velocity.x * dt + this->m_pos.x, m_velocity.y * dt + this->m_pos.y);

    if (m_gamer) {
        char ch[1000];
        sprintf_s(ch, "+++ dt = %f pos = (%f, %f)\n", (float) dt, this->m_pos.x, this->m_pos.y);
        ::OutputDebugStringA(ch);
    }
}

DirectX::XMFLOAT2 GameObject::GetPosition() const {
    return m_pos;
}

void GameObject::SetPosition(DirectX::XMFLOAT2 pos) {
    this->m_pos = pos;
}

void GameObject::SetSpeed(DirectX::XMFLOAT2 speed) {
    this->m_speed = speed;
}

void GameObject::SetVelocity(DirectX::XMFLOAT2 velocity) {
    this->m_velocity = velocity;
}

void GameObject::SetGamer(const Gamer * gamer) {
    m_gamer = gamer;
}

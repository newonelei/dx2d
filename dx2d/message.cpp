#include "message.h"

Message::Message(MessageType type)
	: type_(type)
{
}

Message::Message(const char * payload, unsigned int size) 
{
	SetPayload(payload, size);
}

Message::Message(MessageType type, const char * payload, unsigned int size)
	: Message(type)
{
	SetPayload(payload, size);
}

void Message::SetPayload(const char * payload, unsigned int size)
{
	buffer_.sputn(payload, size);
}

void Message::ToBytes(char* contents)
{
	std::streamsize size = buffer_.pubseekoff(0, std::ios_base::end, std::ios_base::in);
	buffer_.pubseekpos(0, std::ios_base::in);
	buffer_.sgetn(contents, size);
}


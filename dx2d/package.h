﻿#pragma once

class Package {
public:
    // streaming
    Package(const char * payload, unsigned int size);

private:
    const int m_header = 0x87E21;

    const char * m_payload;  // 负载比特流
    unsigned int m_size;     // 负载比特流 bit stream
};
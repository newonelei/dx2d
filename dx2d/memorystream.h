#pragma once
#include <streambuf>
#include <array>
#include <cassert>

template<std::size_t N>
class MemoryStream : public std::streambuf
{
public:
	MemoryStream()
	{
		setbuf(buffer_.data(), buffer_.size());
	}

protected:

	std::basic_streambuf<char, std::char_traits<char>> * setbuf(char_type * str, std::streamsize size) final{
		char_type* begin(str);
		char_type* end(str + size);

		// get : set pointers for read buffer
		setg(begin, begin, end);
		// put : set pointers for write buffer
		setp(begin, end);

		return this;
    }

	// move next pointer with absolute position
	pos_type seekpos(pos_type pos, std::ios_base::openmode const which = std::ios_base::in | std::ios_base::out) final
	{	// change to specified position, according to mode
		switch(which)
		{
		case std::ios_base::in: // read
			if (pos < egptr() - eback())
			{
				setg(eback(), eback() + pos, egptr());
				return pos;
			}
			else
				break;

		case std::ios_base::out: // write
			if(pos < epptr() - pbase())
			{
				setp(pbase(), epptr());
				pbump(pos);
			}
			else
			{
				break;
			}
		default:
			assert(0);
		}
	}

	// move next pointer to the relative position
	pos_type seekoff(off_type off,
		std::ios_base::seekdir dir,
		std::ios_base::openmode const which = std::ios_base::in | std::ios_base::out) final
	{	// change position by offset, according to way and mode
		switch(which)
		{
		case std::ios_base::in:
			if (dir == std::ios_base::cur)
				gbump(static_cast<int>(off));
			else if (dir == std::ios_base::beg)
				setg(eback(), eback() + off, egptr());
			else if (dir == std::ios_base::end)
				setg(eback(), egptr() + off, egptr());
			return gptr() - eback();

		case std::ios_base::out:
			if (dir == std::ios_base::cur)
				pbump(static_cast<int>(off));
			else if (dir == std::ios_base::beg)
				setp(pbase(), pbase() + off, epptr());
			else if (dir == std::ios_base::end)
				setp(pbase(), epptr() + off, epptr());
			return pptr() - pbase();

		default:
			assert(0);
		}
	}

private:
	std::array<char, N> buffer_;
};
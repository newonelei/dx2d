#include "stick.h"
#include "math_helper.h"

Stick::Stick(unsigned int id, int width, int height, D2D1::ColorF color, Knowledge & knowledge)
    : GameObject(id)
    , m_width(width)
    , m_height(height)
    , m_color(color)
    , knowledge(knowledge) {}

void Stick::Shoot() {
    if (this->m_gamer) {
        if (this->m_gamer->IsShootBall()) {
            // shoot ball like bounce
            this->Bounce();
        }
    }
}

void Stick::Bounce() {
    DirectX::XMFLOAT2 ball_pos    = knowledge.m_ball->GetPosition();
    float             ball_radius = knowledge.m_ball->GetRadius();

    Rect rect { this->m_pos.x - this->m_width / 2, this->m_pos.y - this->m_height / 2, this->m_width, this->m_height };
    Circle circle { ball_pos.x, ball_pos.y, ball_radius };
    if (CircleRectangleIntersect(circle, rect)) {
        DirectX::XMFLOAT2 ball_vel = knowledge.m_ball->GetVelocity();
        knowledge.m_ball->SetVelocity(DirectX::XMFLOAT2(ball_vel.x, -1 * ball_vel.y));
	}
}

void Stick::Update(double dt) {
    if (this->m_gamer) {
        int move_dir = m_gamer->GetInputDir();

        if (move_dir != 0) { move_dir = move_dir; }
        m_velocity.x = m_speed.x * move_dir;
        m_velocity.y = 0.0f;

        char ch[1000];
        sprintf_s(ch, "+++ dt = %f, m_velocity = (%f, %f)\n", (float) dt, m_velocity.x, m_velocity.y);
        ::OutputDebugStringA(ch);
    } else {
        // ai
    }
    this->Move(dt);

    // update shoot
    this->Shoot();

    // update bounce
    this->Bounce();
}

void Stick::Render(const RenderContext & render_context) {
    auto render_target = render_context.render_target;
    auto brush         = render_context.brush;
    if (brush) {
        brush->SetColor(m_color);

        D2D1_RECT_F rect = D2D1::Rect((m_pos.x - m_width / 2),    // * render_context.render_target_width,    // left
                                      (m_pos.y - m_height / 2),   // * render_context.render_target_height,  // top
                                      (m_pos.x + m_width / 2),    // * render_context.render_target_width,    // right
                                      (m_pos.y + m_height / 2));  // * render_context.render_target_height); // bottom
        render_target->FillRectangle(rect, brush);
    }
}

#pragma once
#include "gamer.h"
#include "identifier.h"
#include "render_context.h"
#include <DirectXMath.h>

class GameObject {
public:
    GameObject(unsigned int id);
    void         Move(double dt);
    virtual void Render(const RenderContext &) = 0;
    virtual void Update(double dt)             = 0;

    DirectX::XMFLOAT2 GetPosition() const;
    void              SetPosition(DirectX::XMFLOAT2 pos);
    void              SetSpeed(DirectX::XMFLOAT2 speed);

    DirectX::XMFLOAT2 GetVelocity() const { return m_velocity; }

    void SetVelocity(DirectX::XMFLOAT2 velocity);

    void SetGamer(const Gamer * gamer);

protected:
    DirectX::XMFLOAT2 m_pos;
    DirectX::XMFLOAT2 m_speed;
    DirectX::XMFLOAT2 m_velocity;

    Identifier m_id;

    const Gamer * m_gamer;
};
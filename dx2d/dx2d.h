﻿#pragma once

// windows header file
#include <windows.h>

// C runtime file
#include <malloc.h>
#include <math.h>
#include <memory.h>
#include <stdlib.h>
#include <wchar.h>

// direct2D header file
#include "gamer.h"
#include "knowledge.h"
#include "render_context.h"
#include "scene.h"
#include "timer.h"

#include "server.h"
#include "client.h"

#include <d2d1.h>
#include <d2d1helper.h>
#include <dwrite.h>
#include <keyboard.h>
#include <wincodec.h>

template<class Interface> inline void SafeRelease(Interface ** ppInterfaceToRelease) {
    if (*ppInterfaceToRelease != NULL) {
        (*ppInterfaceToRelease)->Release();
        (*ppInterfaceToRelease) = NULL;
    }
}

#ifndef Assert
#if defined(DEBUG) || defined(_DEBUG)
#define Assert(b)                                                                                                                \
    do {                                                                                                                         \
        if (!(b)) { OutputDebugStringA("Assert: " #b "\n"); }                                                                    \
    } while (0)
#else
#define Assert(b)
#endif  // DEBUG || _DEBUG
#endif

#ifndef HINST_THISCOMPONENT
EXTERN_C IMAGE_DOS_HEADER __ImageBase;
#define HINST_THISCOMPONENT ((HINSTANCE) &__ImageBase)
#endif

class D2DDemo {
public:
    typedef enum EGameState { Unstarted, Started, Paused, Exited, Count } GameState;

    D2DDemo(bool isServer);
    ~D2DDemo();

    // Register the window class and call methods for initializing drawing resources
    HRESULT Initialize();

    // Process and dispatch messages
    int MessageLoop();

    void OnRender();

    void OnResize(UINT widht, UINT height);
    void OnPaint();

    void GameInit();
	void TickNetwork();
	void GameLoop();

private:
    static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

    // Initialize device-independent resources
    // 这类设备无关的资源是可以在整个应用程序生命周期使用的，和渲染无关的资源
    HRESULT CreateDeviceIndependentResources();

    // Initialize device-dependent
    // 设备相关的资源都是和渲染有关的资源
    HRESULT CreateDeviceDependentResources();

    // 释放设备相关的资源
    void ReleaseDeviceDependentResources();

    void GameStart();
    void GamePause();
    void GameExit();

private:
    // window hInstance(ID)
    HWND m_hwnd;

    // The ID2D1Factory interface is the starting point for using Direct2D; use an ID2D1Factory to create Direct2D resources.
    ID2D1Factory * m_pDirect2dFactory;

    // window render target
    ID2D1HwndRenderTarget * m_pRenderTarget;

    // 笔刷
    ID2D1SolidColorBrush * m_pSolidBrush;

    // keyboard instance
    std::unique_ptr<DirectX::Keyboard> m_keyboard;

    Timer m_timer;

    Gamer m_gamer;

    RenderContext m_renderContext;

    Scene m_scene;

	GameState m_gameState;

	Server * m_server;
	Client * m_client;
};
#include "client.h"
#include "log.h"

#define WIN

Client::Client(UINT port) {
#ifdef WIN
    WORD    version = MAKEWORD(2, 2);
    WSADATA wsa_data;
    // initialize winsock
    WSAStartup(version, &wsa_data);
#endif

    m_socket = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (m_socket == -1) {
        const char * error_msg = "failed to create a socket for client";
        Log::Output(error_msg);
        return;
    }

    m_port = port;
    memset(&m_udpServerAddr, 0, sizeof(PSOCKADDR_IN));
    m_udpServerAddr.sin_family      = AF_INET;
    m_udpServerAddr.sin_port        = htons(port);
	m_udpServerAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);// inet_addr("192.168.1.255");

    // set socket to use broadcast
    int opt_val                     = 1;
    int opt_len                     = sizeof(opt_val);
    if(setsockopt(m_socket, SOL_SOCKET, SO_BROADCAST, (char *) &opt_val, opt_len))
	{
		Log::Output("Failed to set socket option");
		return;
	}
}

Client::~Client() {
	CloseSocket();

#ifdef WIN
	WSACleanup();
#endif
}

bool Client::Send(const char * message) {
    int ret_code
        = sendto(m_socket, message, strlen(message) + 1, 0, (struct sockaddr *) &m_udpServerAddr, sizeof(m_udpServerAddr));
    if (ret_code < 0) {
        Log::Output("failed to send message");
        return false;
    }

    return true;
}

void Client::CloseSocket() {
    if (m_socket != -1) {
        if (closesocket(m_socket) < 0) Log::Output("Error: close socket failed");

        m_socket = -1;
    }
}

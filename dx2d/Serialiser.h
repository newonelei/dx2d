#pragma once

template<typename T>
class Serialiser
{
	const Serialiser& operator<<(const T& obj);
};
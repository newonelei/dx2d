#include "scene.h"

Scene::Scene(const RenderContext & m_renderContext)
    : m_stickOne(1, 40.f, 10.f, D2D1::ColorF(D2D1::ColorF::Yellow, 1.0f), m_knowledge)
    , m_stickTwo(2, 40.f, 10.f, D2D1::ColorF(D2D1::ColorF::Yellow, 1.0f), m_knowledge)
    , m_ball(99, 10.0f, D2D1::ColorF(D2D1::ColorF::Red, 1.0f))
    , m_renderContext(m_renderContext) {
    m_knowledge.m_ball = &m_ball;
}

void Scene::Init() {
    m_stickOne.SetPosition(DirectX::XMFLOAT2(m_renderContext.render_target_width / 2, m_stickOne.GetHeight() / 2 + 5.0f));
    m_stickTwo.SetPosition(DirectX::XMFLOAT2(m_renderContext.render_target_width / 2,
                                             m_renderContext.render_target_height - 5.0f - m_stickOne.GetHeight() / 2));

    m_stickOne.SetSpeed(DirectX::XMFLOAT2(300.0f, 0.0f));
    m_stickTwo.SetSpeed(DirectX::XMFLOAT2(300.0f, 0.0f));

    m_ball.SetPosition(DirectX::XMFLOAT2(m_renderContext.render_target_width / 2, m_renderContext.render_target_height / 2));
    m_ball.SetVelocity(DirectX::XMFLOAT2(100.0f, 100.f));
}

void Scene::Update(double dt) {
    D2D1_SIZE_F render_target_size = m_renderContext.render_target->GetSize();
    m_ball.Update(dt, render_target_size);

    m_stickOne.Update(dt);
    m_stickTwo.Update(dt);
}

void Scene::Render() {
    m_stickOne.Render(m_renderContext);
    m_stickTwo.Render(m_renderContext);

    m_ball.Render(m_renderContext);
}

void Scene::SetStickOneGamer(const Gamer * gamer) {
    m_stickOne.SetGamer(gamer);
}

void Scene::SetStickTwoGamer(const Gamer * gamer) {
    m_stickOne.SetGamer(gamer);
}

#include "timer.h"

Timer::Timer()
    : m_previousTime(0)
    , m_frequency(0)
    , m_deltaTime(0) {
    LARGE_INTEGER frequency;
    QueryPerformanceFrequency(&frequency);
    m_frequency = static_cast<uint64_t>(frequency.QuadPart);
}

void Timer::Start() {
    this->m_previousTime = Get();
}

void Timer::Tick() {
    uint64_t currentTime = Get();
    this->m_deltaTime    = static_cast<double>(currentTime - this->m_previousTime) / this->m_frequency;
    this->m_previousTime = currentTime;
}

void Timer::End() {}

double Timer::DeltaTime() const {
    return this->m_deltaTime;
}

uint64_t Timer::Get() {
    LARGE_INTEGER count;
    QueryPerformanceCounter(&count);
    return static_cast<uint64_t>(count.QuadPart);
}

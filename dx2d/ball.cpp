#include "ball.h"

Ball::Ball(unsigned int id, float radius, D2D1::ColorF color)
    : GameObject(id)
    , m_radius { radius }
    , m_color(color) {}

void Ball::Update(double dt) {
    this->Move(dt);
}

void Ball::Update(double dt, D2D1_SIZE_F window_size) {
    this->Update(dt);

    if (m_pos.x - m_radius <= 0.0 || m_pos.x + m_radius >= window_size.width) { 
		SetVelocity(DirectX::XMFLOAT2(-1 * m_velocity.x, m_velocity.y));
	}
}

void Ball::Render(const RenderContext & render_context) {
    auto render_target = render_context.render_target;
    auto brush         = render_context.brush;
    if (brush) {
        brush->SetColor(m_color);

        D2D1_ELLIPSE ball = D2D1::Ellipse(D2D1::Point2F(m_pos.x, m_pos.y), m_radius, m_radius);
        render_target->FillEllipse(ball, brush);
    }
}

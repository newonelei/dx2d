#pragma once
#include "memorystream.h"

class Message
{
public:
	enum MessageType{
		CONTROL,
		MOVE,
	};
	Message(MessageType type);
	Message(const char* payload, unsigned int size);
	Message(MessageType type, const char* payload, unsigned int size);

	void SetPayload(const char* payload, unsigned int size);
	void ToBytes(char* contents);

	virtual void Serialise() = 0;

private:
	enum MessageType type_;

	MemoryStream<512> buffer_;
};
#pragma once

#include <d2d1.h>

struct RenderContext
{
	FLOAT render_target_width;
	FLOAT render_target_height;

	ID2D1HwndRenderTarget* render_target;
	ID2D1SolidColorBrush * brush;
};
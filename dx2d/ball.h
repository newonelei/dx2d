#pragma once
#include "gameobject.h"
#include <d2d1.h>
#include <d2d1helper.h>

class Ball : public GameObject {
public:
    Ball(unsigned int id, float radius, D2D1::ColorF color);

    void Update(double dt) override;
    void Update(double dt, D2D1_SIZE_F window_size);
    void Render(const RenderContext &) override;

	float GetRadius() const { return m_radius; }

private:
    float        m_radius;
    D2D1::ColorF m_color;
};
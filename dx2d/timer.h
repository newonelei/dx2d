#pragma once
#include <cstdint>
#include <stdlib.h>
#include <windows.h>

class Timer {
public:
    Timer();

    void Start();
    void Tick();
    void End();

    double DeltaTime() const;

private:
    uint64_t Get();

private:
    uint64_t m_frequency;
    uint64_t m_previousTime;

    double m_deltaTime;
};
#pragma once
#include <DirectXMath.h>

struct Circle{
    float x;
    float y;
    float radius;
};

struct Rect {
    float x;
    float y;
    float width;
    float height;
};

bool CircleRectangleIntersect(Circle circle, Rect rect) {
    struct CircleDistance {
        float x;
        float y;
    } circleDistance;

    circleDistance.x = abs(circle.x - rect.x);
    circleDistance.y = abs(circle.y - rect.y);

    if (circleDistance.x > (rect.width / 2 + circle.radius)) { return false; }
    if (circleDistance.y > (rect.height / 2 + circle.radius)) { return false; }

    if (circleDistance.x <= (rect.width / 2)) { return true; }
    if (circleDistance.y <= (rect.height / 2)) { return true; }

    float cornerDistance_sq = (circleDistance.x - rect.width / 2) * (circleDistance.x - rect.width / 2)
                              + (circleDistance.y - rect.height / 2) * (circleDistance.y - rect.height / 2);

    return (cornerDistance_sq <= (circle.radius * circle.radius));
}